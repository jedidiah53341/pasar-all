@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">
                <form method="POST" action="{{ route('terapkan-tambah-data-landing-page') }}" class="forms-sample">
                    @csrf

                    <h1 class="">Pembuatan Data Landing Page Baru</h1>

                    <div class="form-group">
                            Jenis Data :
                            <select name="flags" class="form-control form-control-alternative"  class="w-100" >
                                <option value="Jumbotron">Jumbotron</option>
                                <option value="Tentang">Tentang</option>
                                <option value="Keunggulan">Keunggulan</option>
                                <option value="Galeri">Galeri</option>
                                <option value="FAQ">FAQ</option>
                                <option value="Kontak">Kontak</option>
                                <option value="Downloads">Downloads</option>

                                <option disabled value="">-- Khusus --</option>
                                <option value="Register Code">Register Code</option>
                                <option value="Lokasi">Lokasi</option>
                                <option value="Spesifikasi">Spesifikasi</option>
                                <option value="Dokumentasi">Dokumentasi</option>
                                <option value="Kontak">Kontak</option>

                            </select>
                            </div>

                    <div class="form-group">
                        Konten :
                        <textarea name="konten"  class="form-control" id="konten"  rows="3" placeholder="Spesifikasi Kios ..."></textarea>
                    </div>

                    <button type="submit" class="btn btn-success">Masukkan Data Baru</button>

                    </form>
                </div>
            </div>
        </div>

@endsection

@push('js')
<!-- Import CKEditor dan terapkan ke textarea dengan id 'cKEditor' -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
CKEDITOR.replace('konten');
</script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush