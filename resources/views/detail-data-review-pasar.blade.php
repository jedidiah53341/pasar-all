@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">

                    <h1 class="">Detail Data Review Pasar</h1> 
                        <div class="form-group">
                        <span class="badge badge-pill badge-primary">Nama Penulis Review : {{ $detailDataReviewPasar->reviewers_name }}</span>
                        <span class="badge badge-pill badge-info">No. Telepon : {{ $detailDataReviewPasar->reviewers_phone_number }}</span>
                        <span class="badge badge-pill badge-success">Email : {{ $detailDataReviewPasar->reviewers_email }}</span>
                        <span class="badge badge-pill badge-warning">Ditulis pada : {{ $detailDataReviewPasar->created_at }}</span>

                        <div class="progress-percentage">
                            {{$detailDataReviewPasar->review_overall_score}}%
                        </div>
                        <div class="progress">
                            <div class="progress-bar bg-default" role="progressbar" aria-valuenow="{{$detailDataReviewPasar->review_overall_score}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$detailDataReviewPasar->review_overall_score}}%;"></div>
                        </div>

                        <span>Komentar : {{ $detailDataReviewPasar->review_comment }}</span>
                        </div>

                    <a href="{{ route('edit-data-review-pasar', ['id' => $detailDataReviewPasar->id ]) }}" ><button type="button" class="btn btn-info w-100 ">Update</button></a>

                    <form class="d-inline" action="{{ route('hapus-data-review-pasar', ['id' => $detailDataReviewPasar->id ]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger mt-3 w-100">Hapus</button>
                    </form>

                    <a href="{{ route('data-review-pasar') }}" ><button type="button" class="btn btn-outlined-primary  mt-3 w-100 ">Kembali Ke Data Review Pasar</button></a>

                </div>
            </div>
        </div>

@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush