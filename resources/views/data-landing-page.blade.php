@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid  mt--8">
    <div class="row mt-3">
            <div class="col-xl-12 mb-5 mb-xl-12">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Landing Page</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{route('tambah-data-landing-page')}}" class="btn btn-primary">Tambah Baru</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Jenis</th>
                                    <th scope="col">Di-update pada</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($daftarDataLandingPage != null) 
                            @foreach ($daftarDataLandingPage as $dataLandingPage)
                                <tr>
                                    <th scope="row">
                                    {{$dataLandingPage->flags}}
                                    </th>
                                    <td>
                                    {{$dataLandingPage->updated_at}}
                                    </td>
                                    <td>
                                    <a href="{{ route('detail-data-landing-page', ['id' => $dataLandingPage->id ]) }}" ><button type="button" class="btn btn-success "><i class="ni ni-zoom-split-in mr-2"></i>Lihat Konten</button></a>
                                    <a href="{{ route('edit-data-landing-page', ['id' => $dataLandingPage->id ]) }}" ><button type="button" class="btn btn-primary "><i class="ni ni-settings mr-2"></i>Update</button></a>

                                    <form class="d-inline" action="{{ route('hapus-data-landing-page', ['id' => $dataLandingPage->id ]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger"><i class="ni ni-scissors mr-2"></i>Hapus</button>
                                    </form>


                                    </td>
                                </tr>
                            @endforeach
                            @else
                            <p></p>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>


        

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush