@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid mt--9">

        
    <div class="row mt-5">
            <div class="col-xl-6 mb-1 mb-xl-0">
                <div class="card bg-gradient-default shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
                                <h2 class="text-white mb-0">Penyewaan Kios</h2>
                            </div>
                            <div class="col">
                                <ul class="nav nav-pills justify-content-end">
                                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales" data-update='{"data":{"datasets":[{"data":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="$" data-suffix="k">
                                        <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                                            <span class="d-none d-md-block">Month</span>
                                            <span class="d-md-none">M</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="$" data-suffix="k">
                                        <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                                            <span class="d-none d-md-block">Week</span>
                                            <span class="d-md-none">W</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart" style="position: relative; height:15vh;">
                            <!-- Chart wrapper -->
                            <canvas id="chart-sales" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card shadow">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-muted ls-1 mb-1">Performance</h6>
                                <h2 class="mb-0">Penambahan Anggota</h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart" style="position: relative; height:15vh;">
                            <canvas id="chart-orders" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="row mt-1 mt-xl-3">
            <div class="col-xl-8 mb-1 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Kios</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{route('tambah-kios')}}" class="btn btn-primary"><i class="ni ni-fat-add mr-2"></i>Tambah Baru</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Kode Kios</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Nama Penyewa</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            @if ($allKiosks != null) 

                            @foreach ($allKiosks as $kiosk)
                                <tr>
                                    <th scope="row">
                                    {{$kiosk->kiosk_code}}
                                    </th>
                                    <td>
                                    @if ($kiosk->kiosk_status == 'Tersedia') 
                                    <span class="badge badge-primary">{{$kiosk->kiosk_status}}</span>
                                    @elseif ($kiosk->kiosk_status == 'Tersewa') 
                                    <span class="badge badge-success">{{$kiosk->kiosk_status}}</span>
                                    @else 
                                    <span class="badge badge-warning">{{$kiosk->kiosk_status}}</span>
                                    @endif
                                    </td>
                                    <td>
                                    @if ($kiosk->kiosk_status == 'Tersedia') 
                                    <span class="badge badge-default">Belum Ada</span>
                                    @elseif ($kiosk->kiosk_status == 'Tersewa') 
                                    {{$kiosk->kiosk_renter_name}}
                                    @else
                                    <span class="badge badge-danger">Belum Tersedia</span>
                                    @endif
                                    </td>
                                    <td>
                                    <a href="{{ route('detail-kios', ['id' => $kiosk->id ]) }}" ><button type="button" class="btn btn-success btn-sm "><i class="ni ni-zoom-split-in mr-2"></i>Detail</button></a>
                                    <a href="{{ route('edit-kios', ['id' => $kiosk->id ]) }}" ><button type="button" class="btn btn-info btn-sm "><i class="ni ni-settings mr-2"></i>Update</button></a>

                                    <form class="d-inline" action="{{ route('hapus-kios', ['id' => $kiosk->id ]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm"><i class="ni ni-scissors mr-2"></i>Hapus</button>
                                    </form>


                                    </td>
                                </tr>
                            @endforeach

                            @else
                            <p></p>
                            @endif
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
            <div class="card p-2" >
            <div class="nav-wrapper">
    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
        <li class="nav-item">
            <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-building mr-2"></i>Lokasi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-badge mr-2"></i>Spesifikasi</a>
        </li>
    </ul>
</div>
<div class="card shadow">
    <div class="card-body">
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                @if ($landingPageLokasi != null) 
                {!! $landingPageLokasi->konten !!}
                @else
                <p></p>
                @endif
                
            </div>
            <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                @if ($landingPageSpesifikasi != null) 
                {!! $landingPageSpesifikasi->konten !!}
                @else
                <p></p>
                @endif
            </div>
        </div>
    </div>
</div>
            </div>
            </div>
        </div>


        

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush