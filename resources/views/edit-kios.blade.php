@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">
                <form method="POST" action="{{ route('terapkan-edit-kios', ['id' => $dataDetailKios->id ]) }}" class="forms-sample">
                    @method('PUT')
                    @csrf

                    <h1 class="">Edit Data Kios</h1>
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            Kode Kios :
                            <input value="{{ $dataDetailKios->kiosk_code }}" name="kiosk_code" type="text" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Kode Kios">
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            Harga Sewa :
                            <input value="{{ $dataDetailKios->kiosk_rent_price }}"  name="kiosk_rent_price"  type="number" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Harga Sewa">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            Status Kios :
                            <select name="kiosk_status" class="form-control form-control-alternative"  class="w-100" >
                                
                            @if ($dataDetailKios->kiosk_status == 'Tersedia') 
                                <option value="Tersedia" selected >Tersedia</option>
                            @else
                                <option value="Tersedia" >Tersedia</option>
                            @endif

                            @if ($dataDetailKios->kiosk_status == 'Tersewa') 
                                <option value="Tersewa" selected >Tersewa</option>
                            @else
                                <option value="Tersewa" >Tersewa</option>
                            @endif

                            @if ($dataDetailKios->kiosk_status == 'Tidak Tersedia') 
                                <option value="Tidak Tersedia" selected >Tidak Tersedia</option>
                            @else
                                <option value="Tidak Tersedia" >Tidak Tersedia</option>
                            @endif


                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                Spesifikasi Kios :
                                <textarea  name="kiosk_specification"  class="form-control" id="kiosk_specification" rows="3" placeholder="Spesifikasi Kios ...">{{ $dataDetailKios->kiosk_specification }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                Gambar Kios :
                                <textarea  name="kiosk_image"  class="form-control" id="kiosk_image" rows="3" placeholder="Spesifikasi Kios ...">{{ $dataDetailKios->kiosk_image }}</textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                Nama Penyewa :
                                <input value="{{ $dataDetailKios->kiosk_renter_name }}"  name="kiosk_renter_name"  type="text" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Nama Penyewa">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                No. Telepon Penyewa :
                                <input value="{{ $dataDetailKios->kiosk_renter_phone }}"  name="kiosk_renter_phone"  type="text" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="No. Telepon Penyewa">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                Foto Penyewa :
                                <textarea   name="kiosk_renter_photo_image"  class="form-control" id="kiosk_renter_photo_image" rows="3" placeholder="Foto Penyewa ...">{{ $dataDetailKios->kiosk_renter_photo_image }}</textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                Foto KTP Penyewa :
                                <textarea  name="kiosk_renter_ktp_image"  class="form-control" id="kiosk_renter_ktp_image" rows="3" placeholder="Foto KTP Penyewa ...">{{ $dataDetailKios->kiosk_renter_ktp_image }}</textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success w-100">Terapkan Update</button>
                    </form>

                    <a href="{{ route('dashboard') }}" ><button type="button" class="btn btn-outlined-primary  mt-3 w-100 ">Kembali Ke Dashboard</button></a>

                </div>
            </div>
        </div>

@endsection

@push('js')
<!-- Import CKEditor dan terapkan ke textarea dengan id 'cKEditor' -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
CKEDITOR.replace('kiosk_specification');
CKEDITOR.replace('kiosk_image');
CKEDITOR.replace('kiosk_renter_photo_image');
CKEDITOR.replace('kiosk_renter_ktp_image');
</script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush