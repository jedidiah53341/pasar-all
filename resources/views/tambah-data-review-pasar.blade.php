@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">
                <form method="POST" action="{{ route('terapkan-tambah-data-review-pasar-dari-landing-page') }}" class="forms-sample">
                    @csrf

                    <h1 class="">Pembuatan Data Review Pasar Baru</h1>

                    <div class="form-group">
                    Nama Penulis :
                    <input name="reviewers_name" type="text" class="form-control form-control-alternative" placeholder="Nama Penulis">
                    </div>

                    <div class="form-group">
                    No. Telepon :
                    <input name="reviewers_phone_number" type="text" class="form-control form-control-alternative" placeholder="No. Telepon">
                    </div>

                    <div class="form-group">
                    Email :
                    <input name="reviewers_email" type="email" class="form-control form-control-alternative" placeholder="Email">
                    </div>

                    <div class="form-group">
                    Skor :
                    <input name="review_overall_score" type="number" class="form-control form-control-alternative" placeholder="Skor">
                    </div>

                    <div class="form-group">
                    Komentar :
                    <input name="review_comment" type="text" class="form-control form-control-alternative" placeholder="Komentar">
                    </div>

                    <button type="submit" class="btn btn-success">Masukkan Data Baru</button>

                    </form>
                </div>
            </div>
        </div>

@endsection