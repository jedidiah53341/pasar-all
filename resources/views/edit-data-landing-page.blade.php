@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">
                <form method="POST" action="{{ route('terapkan-edit-data-landing-page', ['id' => $detailDataLandingPage->id ]) }}" class="forms-sample">
                    @method('PUT')
                    @csrf

                    <h1 class="">Edit Data Landing Page</h1>
                    <div class="form-group">
                            Jenis Data :
                            <input hidden name="flags" value="{{ $detailDataLandingPage->flags }}" class="form-control form-control-alternative"  class="w-100" />
                            <input disabled name="flags" value="{{ $detailDataLandingPage->flags }}" class="form-control form-control-alternative"  class="w-100" />
                            </div>

                    <div class="form-group">
                        Konten :
                        <textarea name="konten"  class="form-control" id="konten"  rows="3" placeholder="Spesifikasi Kios ...">{{ $detailDataLandingPage->konten }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-success">Terapkan Edit</button>
                    </form>

                    <a href="{{ route('data-landing-page') }}" ><button type="button" class="btn btn-outlined-primary  mt-3 w-100 ">Kembali Ke Daftar Data Landing Page</button></a>

                </div>
            </div>
        </div>

@endsection

@push('js')
<!-- Import CKEditor dan terapkan ke textarea dengan id 'cKEditor' -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
CKEDITOR.replace('konten');
</script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush