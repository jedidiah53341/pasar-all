@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid  mt--8">
    <div class="row mt-3">
            <div class="col-xl-12 mb-5 mb-xl-12">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Review Pasar</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{route('tambah-data-review-pasar')}}" class="btn btn-primary">Tambah Baru</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Nama Reviewer</th>
                                    <th scope="col">No. Telp</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Skor</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($daftarDataReviewPasar != null) 
                            @foreach ($daftarDataReviewPasar as $dataReviewPasar)
                                <tr>
                                    <th scope="row">
                                    {{$dataReviewPasar->reviewers_name}}
                                    </th>
                                    <td>
                                    {{$dataReviewPasar->reviewers_phone_number}}
                                    </td>
                                    <td>
                                    {{$dataReviewPasar->reviewers_email}}
                                    </td>
                                    <td>
                                        <div class="progress-percentage">
                                            {{$dataReviewPasar->review_overall_score}}%
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar bg-default" role="progressbar" aria-valuenow="{{$dataReviewPasar->review_overall_score}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$dataReviewPasar->review_overall_score}}%;"></div>
                                        </div>
                                    </td>
                                    <td>
                                    <a href="{{ route('detail-data-review-pasar', ['id' => $dataReviewPasar->id ]) }}" ><button type="button" class="btn btn-sm btn-success "><i class="ni ni-zoom-split-in mr-2"></i>Lihat Konten</button></a>
                                    <a href="{{ route('edit-data-review-pasar', ['id' => $dataReviewPasar->id ]) }}" ><button type="button" class="btn btn-sm btn-primary "><i class="ni ni-settings mr-2"></i>Update</button></a>

                                    <form class="d-inline" action="{{ route('hapus-data-review-pasar', ['id' => $dataReviewPasar->id ]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger"><i class="ni ni-scissors mr-2"></i>Hapus</button>
                                    </form>


                                    </td>
                                </tr>
                            @endforeach
                            @else
                            <p></p>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>


        

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush