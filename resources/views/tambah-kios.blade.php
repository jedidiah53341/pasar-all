@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">
                <form method="POST" action="{{ route('terapkan-tambah-kios') }}" class="forms-sample">
                    @csrf

                    <h1 class="">Pembuatan Data Kios Baru</h1>
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            Kode Kios :
                            <input name="kiosk_code" type="text" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Kode Kios">
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            Harga Sewa :
                            <input name="kiosk_rent_price"  type="number" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Harga Sewa">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            Status Kios :
                            <select name="kiosk_status" class="form-control form-control-alternative"  class="w-100" >
                                <option value="Tersedia">Tersedia</option>
                                <option value="Tersewa">Tersewa</option>
                                <option value="Tidak Tersedia">Tidak Tersedia</option>
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                Spesifikasi Kios :
                                <textarea name="kiosk_specification"  class="form-control" id="kiosk_specification"  rows="3" placeholder="Spesifikasi Kios ..."></textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                Gambar Kios :
                                <textarea name="kiosk_image"  class="form-control" id="kiosk_image"  rows="3" placeholder="Spesifikasi Kios ..."></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                Nama Penyewa :
                                <input name="kiosk_renter_name"  type="text" class="form-control form-control-alternative"  placeholder="Nama Penyewa">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                No. Telepon Penyewa :
                                <input name="kiosk_renter_phone"  type="text" class="form-control form-control-alternative" placeholder="No. Telepon Penyewa">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                Foto Penyewa :
                                <textarea name="kiosk_renter_photo_image"  class="form-control" id="kiosk_renter_photo_image"  rows="3" placeholder="Foto Penyewa ..."></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                Foto KTP Penyewa :
                                <textarea name="kiosk_renter_ktp_image"  class="form-control" id="kiosk_renter_ktp_image" rows="3" placeholder="Foto KTP Penyewa ..."></textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success">Masukkan Data Baru</button>

                    </form>
                </div>
            </div>
        </div>

@endsection

@push('js')
<!-- Import CKEditor dan terapkan ke textarea dengan id 'cKEditor' -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
CKEDITOR.replace('kiosk_specification');
CKEDITOR.replace('kiosk_image');
CKEDITOR.replace('kiosk_renter_photo_image');
CKEDITOR.replace('kiosk_renter_ktp_image');
</script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush