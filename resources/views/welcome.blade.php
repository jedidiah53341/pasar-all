<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Pasar All</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
        <link href="{{ asset('welcome/css/styles.css') }}" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav" style="padding:1.5%;">
            <div class="container px-3">
                <a class="navbar-brand fw-bold" href="#page-top">Pasar All</a>
                <button style="border:none;font-size:150%;" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    
                    <i class="bi-list"></i>
                </button>
                <div class="collapse navbar-collapse my-7 my-xl-0" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
                        <li class="nav-item"><a class="nav-link " href="#tentang">Tentang</a></li>
                        <li class="nav-item"><a class="nav-link " href="#keunggulan">Keunggulan</a></li>
                        <li class="nav-item"><a class="nav-link " href="#galeri">Galeri</a></li>
                        <li class="nav-item"><a class="nav-link " href="#faq">FAQ</a></li>
                    </ul>

                    @if (!Auth::guest())
                        <a href="{{route('dashboard')}}" >
                        <button class="btn  rounded-pill px-3 mr-3 mb-2 mb-lg-0" style="background-color:hsl(220,100%,70%);">
                                <i class="bi bi-display me-2"></i>
                                <span class="small">Dashboard</span>
                        </button>
                        </a>
                    @else
                        <a href="{{route('login')}}" >
                        <button class="btn  rounded-pill px-3 mb-2 mr-3 mb-lg-0"  style="background-color:hsl(220,100%,70%);">
                            <span class="d-flex align-items-center">
                                <i class="bi-person me-2"></i>
                                <span class="small">Login</span>
                            </span>
                        </button>
                        </a>
                    @endif

                </div>
            </div>
        </nav>
        <!-- Jumbotron header-->
        <section id="jumbotron" class="cta" style="box-shadow: 5px 5px 8px black;color:white;background-image: url({{asset('assets/pasar.jpg')}})" >
            <div class="cta-content">
                <div class="container px-5">
                        <div class="mb-5 mb-lg-0 text-center text-lg-start">
                        @if ($landingPageJumbotron != null) 
                        {!! $landingPageJumbotron->konten !!}
                        @else
                        <p></p>
                        @endif
                        </div>
                </div>
            </div>
        </section>
        <!-- About aside-->
        <aside id="tentang" class="text-center py-7 bg-skew " style="box-shadow: 3px 3px 8px black;background-color:hsl(160,100%,40%);" >
                <div class="container px-3 bg-skew-reverse ">
                        <div class="mb-lg-0 text-center text-lg-start">
                        @if ($landingPageTentang != null) 
                        {!! $landingPageTentang->konten !!}
                        @else
                        <p></p>
                        @endif
                        </div>
                </div>
        </aside>
        <!-- Features section-->
        <section id="keunggulan"  >
            <div class="container px-5">

            @if ($landingPageKeunggulan != null) 
            {!! $landingPageKeunggulan->konten !!}
            @else
            <p></p>
            @endif

                </div>
            </div>
        </section>
        <!-- Gallery section-->
        <section id="galeri" class="bg-light  ">
            <div class="container px-5">

            @if ($landingPageGaleri != null) 
            {!! $landingPageGaleri->konten !!}
            @else
            <p></p>
            @endif

            </div>
        </section>
        <!-- Ratings section-->
        <section id="ratings" class="bg-skew" style="background-color:hsl(140,100%,35%);">
            <div class="container px-5 bg-skew-reverse ">
                <h2 class="text-white" >Review Mereka</h2>

            <div class="row">
            @if ($landingPageRatings != null) 
            @foreach ($landingPageRatings as $dataReviewPasar)

            <div class="col-xl-3 mb-2">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{$dataReviewPasar->reviewers_name}}</h5>
                <p class="card-text">"{{$dataReviewPasar->review_comment }}"</p><span class="btn btn-success ">{{$dataReviewPasar->review_overall_score }}%</span>

                <span class="badge badge-primary text-black">{{$dataReviewPasar->created_at }}</span>

            </div>
            </div>
            </div>
            @endforeach
            @else
            <p></p>
            @endif

            </div>
        </section>
        <!-- FAQ section-->
        <section id="faq" class="cta" style="box-shadow: 5px 5px 8px black;color:white" >
            <div class="cta-content">
                <div class="container px-5">

            @if ($landingPageFAQ != null) 
            {!! $landingPageFAQ->konten !!}
            @else
            <p></p>
            @endif

                </div>
            </div>
        </section>

        <!-- Footer-->
        <footer class="bg-black text-center py-1">
            <div class="container">
                <div class="text-white-50 small">
                    <div class="mb-2">&copy; Pasar All 2022.</div>
                </div>
            </div>
        </footer>

        <!-- Toggle untuk dialog kontak -->
        <p data-toggle="modal" data-target="#kontak-notification" id="landing-page-contact-button" >
        <i class="bi-phone me-2 "></i>
        <span class="small ">Kontak Kami</span>
        </p>

        <!-- Toggle untuk tulis review -->
        <p data-toggle="modal" data-target="#modal-notification" id="landing-page-review-button" >
        <i class="bi-chat-text-fill me-2 "></i>
        <span class="small ">Tulis Review</span>
        </p>

        <!-- Toggle untuk downloads -->
        <p data-toggle="modal" data-target="#downloads-notification" id="landing-page-downloads-button" >
        <span class="small ">Unduhan</span>
        </p>

        <!-- Dialog untuk kontak  -->
        <div class="modal fade" id="kontak-notification" tabindex="-1" role="dialog" aria-labelledby="kontak-notification" aria-hidden="true">
            <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
                <div class="modal-content bg-white">
                    
                    <div class="modal-header m-0 p-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="text-black" aria-hidden="true">×</span>
                        </button>
                    </div>
                    
                    <div class="modal-body text-black">

                    @if ($landingPageKontak != null) 
                        {!! $landingPageKontak->konten !!}
                        @else
                        <p></p>
                    @endif
                    </div>
                    
                </div>
            </div>
        </div>
     <!-- Dialog untuk kontak -->

        <!-- Dialog untuk tulis review -->
        <div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-success">
        	
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-notification">Tulis Review Baru</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
                <p>Silahkan tulis nama anda, telepon, dan email. Masukkan skor dari rentang 0-100, dan komentar jika dibutuhkan.</p>

                <form method="POST" action="{{ route('terapkan-tambah-data-review-pasar-dari-landing-page') }}" class="forms-sample">
                    @csrf

                <div class="">
                    Nama Penulis :
                    <input name="reviewers_name" type="text" class="form-control form-control-alternative" placeholder="Nama Penulis">
                    </div>

                    <div class="row mt-2">
                    <div class="col-md-5">
                    No. Telepon :
                    <input name="reviewers_phone_number" type="text" class="form-control form-control-alternative" placeholder="No. Telepon">
                    </div>

                    <div class="col-md-7">
                    Email :
                    <input name="reviewers_email" type="email" class="form-control form-control-alternative" placeholder="Email">
                    </div>
                    </div>

                    <div class="row mt-2">
                    <div class="col-md-5">
                    Skor :
                    <input name="review_overall_score" min="1" max="100" type="number" class="form-control form-control-alternative" placeholder="Skor">
                    </div>

                    <div class="col-md-7">
                    Komentar :
                    <input name="review_comment" type="text" class="form-control form-control-alternative" placeholder="Komentar">
                    </div>
                    </div>

                    <p>Masukkanlah data identitas yang dapat kami kenali sebagai pengunjung Pasar All. Jika admin tak dapat mengenali identitas anda, review anda dapat kami hapus.</p>


                    <button type="submit" class="btn btn-primary w-100">Masukkan Review</button>
                </form>
                
            </div>
            
        </div>
    </div>
</div>
     <!-- Dialog untuk tulis review -->

            <!-- Dialog untuk downloads  -->
            <div class="modal fade" id="downloads-notification" tabindex="-1" role="dialog" aria-labelledby="downloads-notification" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
            <div class="modal-content bg-gradient-primary">
                
                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-notification">Unduhan</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                
                <div class="modal-body">

                @if ($landingPageDownloads != null) 
                    {!! $landingPageDownloads->konten !!}
                    @else
                    <p></p>
                @endif
                </div>
                
            </div>
        </div>
    </div>
     <!-- Dialog untuk downloads -->

        <!-- Bootstrap core JS-->
        <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('welcome/js/scripts.js') }}"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>

    </body>
</html>
