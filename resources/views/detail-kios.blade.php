@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">

                    <h1 class="">Detail Kios</h1>
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                            Kode Kios : {{ $dataDetailKios->kiosk_code }}
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            Harga Sewa : {{ $dataDetailKios->kiosk_rent_price }}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            Status Kios : {{ $dataDetailKios->kiosk_status }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                Spesifikasi Kios : {!! $dataDetailKios->kiosk_specification !!}
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                Gambar Kios : {!! $dataDetailKios->kiosk_image !!}
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                Nama Penyewa : {{ $dataDetailKios->kiosk_renter_name }}
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                No. Telepon Penyewa : {{ $dataDetailKios->kiosk_renter_phone }}
                            </div>
                        </div>
                    </div>

                    <div class="row" >
                        <div class="col-md-6">
                            <div  >
                                Foto Penyewa : {!!  $dataDetailKios->kiosk_renter_photo_image !!}
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                Foto KTP Penyewa : {!! $dataDetailKios->kiosk_renter_ktp_image !!}
                            </div>
                        </div>
                    </div>

                    
                    <a href="{{ route('edit-kios', ['id' => $dataDetailKios->id ]) }}" ><button type="button" class="btn btn-info w-100 ">Update</button></a>

                    <form class="d-inline" action="{{ route('hapus-kios', ['id' => $dataDetailKios->id ]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger mt-3 w-100">Hapus</button>
                    </form>

                    <a href="{{ route('dashboard') }}" ><button type="button" class="btn btn-outlined-primary  mt-3 w-100 ">Kembali Ke Dashboard</button></a>

                </div>
            </div>
        </div>

@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush