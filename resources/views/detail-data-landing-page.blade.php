@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">

                    <h1 class="">Detail Data Landing Page</h1> 
                        <div class="form-group">
                        <span class="badge badge-pill badge-success">Jenis : {{ $detailDataLandingPage->flags }}</span>
                        <span class="badge badge-pill badge-info">Di-update pada : {{ $detailDataLandingPage->updated_at }}</span>
                        </div>

                        <div class="form-group">
                        {!! $detailDataLandingPage->konten !!}
                        </div>

                    <a href="{{ route('edit-data-landing-page', ['id' => $detailDataLandingPage->id ]) }}" ><button type="button" class="btn btn-info w-100 ">Update</button></a>

                    <form class="d-inline" action="{{ route('hapus-data-landing-page', ['id' => $detailDataLandingPage->id ]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger mt-3 w-100">Hapus</button>
                    </form>

                    <a href="{{ route('data-landing-page') }}" ><button type="button" class="btn btn-outlined-primary  mt-3 w-100 ">Kembali Ke Data Landing Page</button></a>

                </div>
            </div>
        </div>

@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush