@extends('layouts.app')

@section('content')
    
    <div class="container-fluid">

        
    <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card p-3">
                <form method="POST" action="{{ route('terapkan-edit-data-review-pasar', ['id' => $detailDataReviewPasar->id ]) }}" class="forms-sample">
                    @method('PUT')
                    @csrf

                    <h1 class="">Edit Data Review Pasar</h1>
                    <div class="form-group">
                    Nama Penulis :
                    <input value="{{ $detailDataReviewPasar->reviewers_name }}" name="reviewers_name" type="text" class="form-control form-control-alternative" placeholder="Nama Penulis">
                    </div>

                    <div class="form-group">
                    No. Telepon :
                    <input value="{{ $detailDataReviewPasar->reviewers_phone_number }}" name="reviewers_phone_number" type="text" class="form-control form-control-alternative" placeholder="No. Telepon">
                    </div>

                    <div class="form-group">
                    Email :
                    <input value="{{ $detailDataReviewPasar->reviewers_email }}" name="reviewers_email" type="email" class="form-control form-control-alternative" placeholder="Email">
                    </div>

                    <div class="form-group">
                    Skor :
                    <input value="{{ $detailDataReviewPasar->review_overall_score }}" name="review_overall_score" type="number" class="form-control form-control-alternative" placeholder="Skor">
                    </div>

                    <div class="form-group">
                    Komentar :
                    <input value="{{ $detailDataReviewPasar->review_comment }}" name="review_comment" type="text" class="form-control form-control-alternative" placeholder="Komentar">
                    </div>

                    <button type="submit" class="btn btn-success">Terapkan Edit</button>
                    </form>

                    <a href="{{ route('data-review-pasar') }}" ><button type="button" class="btn btn-outlined-primary  mt-3 w-100 ">Kembali Ke Daftar Data Landing Page</button></a>

                </div>
            </div>
        </div>

@endsection

@push('js')
<!-- Import CKEditor dan terapkan ke textarea dengan id 'cKEditor' -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
CKEDITOR.replace('konten');
</script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush