<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosks', function (Blueprint $table) {
            $table->id();
            $table->string('kiosk_code')->nullable();;
            $table->longtext('kiosk_image')->nullable();;
            $table->longtext('kiosk_specification')->nullable();;
            $table->double('kiosk_rent_price')->nullable();;
            $table->string('kiosk_status')->nullable();;
            $table->string('kiosk_renter_name')->nullable();;
            $table->string('kiosk_renter_phone')->nullable();;
            $table->longtext('kiosk_renter_photo_image')->nullable();;
            $table->longtext('kiosk_renter_ktp_image')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosks');
    }
};
