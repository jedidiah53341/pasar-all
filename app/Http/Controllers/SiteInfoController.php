<?php

namespace App\Http\Controllers;

use App\Models\SiteInfo;
use App\Http\Requests\StoreSiteInfoRequest;
use App\Http\Requests\UpdateSiteInfoRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class SiteInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function landingPage()
    {
        $landingPageJumbotron = DB::table('site_infos')->where('flags', 'Jumbotron')->first();
        $landingPageTentang = DB::table('site_infos')->where('flags', 'Tentang')->first();
        $landingPageKeunggulan = DB::table('site_infos')->where('flags', 'Keunggulan')->first();
        $landingPageGaleri = DB::table('site_infos')->where('flags', 'Galeri')->first();
        $landingPageRatings = DB::table('user_reviews')->orderBy('created_at', 'desc')->limit(4)->get();
        $landingPageFAQ = DB::table('site_infos')->where('flags', 'FAQ')->first();
        $landingPageKontak = DB::table('site_infos')->where('flags', 'Kontak')->first();
        $landingPageDownloads = DB::table('site_infos')->where('flags', 'Downloads')->first();

        return view('welcome', [
            'landingPageJumbotron' => $landingPageJumbotron,
            'landingPageTentang' => $landingPageTentang,
            'landingPageKeunggulan' => $landingPageKeunggulan,
            'landingPageGaleri' => $landingPageGaleri,
            'landingPageRatings' => $landingPageRatings,
            'landingPageFAQ' => $landingPageFAQ,
            'landingPageKontak' => $landingPageKontak,
            'landingPageDownloads' => $landingPageDownloads,
        ]);
    }

    public function tulisReviewDariLandingPage(Request $request)
    {
        $entry = array();
        $entry['reviewers_name'] = $request->input('reviewers_name');
        $entry['reviewers_phone_number'] = $request->input('reviewers_phone_number');
        $entry['reviewers_email'] = $request->input('reviewers_email');
        $entry['review_overall_score'] = $request->input('review_overall_score');
        $entry['review_comment'] = $request->input('review_comment');
        $entry['created_at'] = Carbon::now('Asia/Bangkok');
        $entry['updated_at'] = Carbon::now('Asia/Bangkok'); 
        DB::table('user_reviews')->insert($entry);

        return redirect()->route('landing-page');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSiteInfoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSiteInfoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiteInfo  $siteInfo
     * @return \Illuminate\Http\Response
     */
    public function show(SiteInfo $siteInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiteInfo  $siteInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteInfo $siteInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSiteInfoRequest  $request
     * @param  \App\Models\SiteInfo  $siteInfo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSiteInfoRequest $request, SiteInfo $siteInfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiteInfo  $siteInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteInfo $siteInfo)
    {
        //
    }
}
