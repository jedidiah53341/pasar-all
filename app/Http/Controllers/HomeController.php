<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */



    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */



    public function index()
    {
        return view('dashboard');
    }

    public function dashboard()
    {
        $allKiosks = DB::table('kiosks')->get();
        $totalKios = DB::table('kiosks')->count();
        $totalKiosTersewa = DB::table('kiosks')->where('kiosk_status', 'Tersewa')->count();
        $rataanSewa = DB::table('kiosks')->avg('kiosk_rent_price');
        $sewaTerbaru = DB::table('kiosks')->orderBy('created_at', 'desc')->first();
        $landingPageLokasi = DB::table('site_infos')->where('flags', 'Lokasi')->first();
        $landingPageSpesifikasi = DB::table('site_infos')->where('flags', 'Spesifikasi')->first();

        return view('dashboard', [
            'allKiosks' => $allKiosks,
            'totalKios' => $totalKios,
            'totalKiosTersewa' => $totalKiosTersewa,
            'rataanSewa' => $rataanSewa,
            'sewaTerbaru' => $sewaTerbaru,
            'landingPageLokasi' => $landingPageLokasi,
            'landingPageSpesifikasi' => $landingPageSpesifikasi,
        ]);
    }

    public function detailKios($id)
    {
        $dataDetailKios = DB::table('kiosks')->where('id', $id)->first();
        return view('detail-kios', [
            'dataDetailKios' => $dataDetailKios,
        ]);
    }

    public function tambahKios()
    {
        return view('tambah-kios', [
        ]);
    }

    public function terapkanTambahKios(Request $request)
    {

        // Olah Data Gambar Foto Kios yang diupload menggunakan CKEditor
        $kiosk_image = str_replace("<p>","",$request->input('kiosk_image'));
        $kiosk_image =  str_replace("</p>","",$kiosk_image);
        $kiosk_image =  str_replace("<img src=","<img style='width: 98%;object-fit:contain' src=",$kiosk_image);

        // Olah Data Gambar Foto Penyewa yang diupload menggunakan CKEditor
        $kiosk_renter_photo_image = str_replace("<p>","",$request->input('kiosk_renter_photo_image'));
        $kiosk_renter_photo_image =  str_replace("</p>","",$kiosk_renter_photo_image);
        $kiosk_renter_photo_image =  str_replace("<img src=","<img style='width: 98%;object-fit:contain' src=",$kiosk_renter_photo_image);

        // Olah Data Gambar Foto KTP Penyewa yang diupload menggunakan CKEditor
        $kiosk_renter_ktp_image = str_replace("<p>","",$request->input('kiosk_renter_ktp_image'));
        $kiosk_renter_ktp_image =  str_replace("</p>","",$kiosk_renter_ktp_image);
        $kiosk_renter_ktp_image =  str_replace("<img src=","<img style='width: 98%;object-fit:contain' src=",$kiosk_renter_ktp_image);

        $entry = array();
        $entry['kiosk_code'] = $request->input('kiosk_code');
        $entry['kiosk_image'] = $kiosk_image;
        $entry['kiosk_specification'] = $request->input('kiosk_specification');
        $entry['kiosk_rent_price'] = $request->input('kiosk_rent_price');
        $entry['kiosk_status'] = $request->input('kiosk_status');
        $entry['kiosk_renter_name'] = $request->input('kiosk_renter_name');
        $entry['kiosk_renter_phone'] = $request->input('kiosk_renter_phone');
        $entry['kiosk_renter_photo_image'] = $kiosk_renter_photo_image;
        $entry['kiosk_renter_ktp_image'] = $kiosk_renter_ktp_image;
        $entry['created_at'] = Carbon::now('Asia/Bangkok');
        $entry['updated_at'] = Carbon::now('Asia/Bangkok'); 
        DB::table('kiosks')->insert($entry);

        return redirect()->route('dashboard');
    }

    public function editKios($id)
    {
        $dataDetailKios = DB::table('kiosks')->where('id', $id)->first();
        return view('edit-kios', [
            'dataDetailKios' => $dataDetailKios,
        ]);
    }

    public function terapkanEditKios(Request $request, $id)
    {

        // Olah Data Gambar Foto Kios yang diupload menggunakan CKEditor
        $kiosk_image = str_replace("<p>","",$request->input('kiosk_image'));
        $kiosk_image =  str_replace("</p>","",$kiosk_image);
        $kiosk_image =  str_replace("<img src=","<img style='width: 98%;object-fit:contain' src=",$kiosk_image);

        // Olah Data Gambar Foto Penyewa yang diupload menggunakan CKEditor
        $kiosk_renter_photo_image = str_replace("<p>","",$request->input('kiosk_renter_photo_image'));
        $kiosk_renter_photo_image =  str_replace("</p>","",$kiosk_renter_photo_image);
        $kiosk_renter_photo_image =  str_replace("<img src=","<img style='width: 98%;object-fit:contain' src=",$kiosk_renter_photo_image);

        // Olah Data Gambar Foto KTP Penyewa yang diupload menggunakan CKEditor
        $kiosk_renter_ktp_image = str_replace("<p>","",$request->input('kiosk_renter_ktp_image'));
        $kiosk_renter_ktp_image =  str_replace("</p>","",$kiosk_renter_ktp_image);
        $kiosk_renter_ktp_image =  str_replace("<img src=","<img style='width: 98%;object-fit:contain' src=",$kiosk_renter_ktp_image);


        DB::table('kiosks')->where('id', $id)->update([
            'kiosk_code' => $request->input('kiosk_code'),
            'kiosk_image' => $kiosk_image,
            'kiosk_specification' => $request->input('kiosk_specification'),
            'kiosk_rent_price' => $request->input('kiosk_rent_price'),
            'kiosk_status' => $request->input('kiosk_status'),
            'kiosk_renter_name' => $request->input('kiosk_renter_name'),
            'kiosk_renter_phone' => $request->input('kiosk_renter_phone'),
            'kiosk_renter_photo_image' => $kiosk_renter_photo_image,
            'kiosk_renter_ktp_image' => $request->input('kiosk_renter_ktp_image'),
            'updated_at' => Carbon::now('Asia/Bangkok')]);

            return redirect()->route('dashboard');
    }


    public function hapusKios($id)
    {
       DB::table('kiosks')->where('id', $id)->delete();
       return redirect()->route('dashboard');
    }

    public function dataLandingPage()
    {
        $daftarDataLandingPage = DB::table('site_infos')->get();
        $totalKios = DB::table('kiosks')->count();
        $totalKiosTersewa = DB::table('kiosks')->where('kiosk_status', 'tersewa')->count();
        $rataanSewa = DB::table('kiosks')->avg('kiosk_rent_price');
        $sewaTerbaru = DB::table('kiosks')->orderBy('created_at', 'desc')->first();
        return view('data-landing-page', [
            'daftarDataLandingPage' => $daftarDataLandingPage,
            'totalKios' => $totalKios,
            'totalKiosTersewa' => $totalKiosTersewa,
            'rataanSewa' => $rataanSewa,
            'sewaTerbaru' => $sewaTerbaru,
        ]);
    }

    public function detailDataLandingPage($id)
    {
        $detailDataLandingPage = DB::table('site_infos')->where('id', $id)->first();
        return view('detail-data-landing-page', [
            'detailDataLandingPage' => $detailDataLandingPage,
        ]);
    }

    public function tambahDataLandingPage()
    {
        return view('tambah-data-landing-page', [
        ]);
    }

    public function terapkanTambahDataLandingPage(Request $request)
    {
        $entry = array();
        $entry['flags'] = $request->input('flags');
        $entry['konten'] = $request->input('konten');
        $entry['created_at'] = Carbon::now('Asia/Bangkok');
        $entry['updated_at'] = Carbon::now('Asia/Bangkok'); 
        DB::table('site_infos')->insert($entry);

        return redirect()->route('data-landing-page');
    }

    public function editDataLandingPage($id)
    {
        $detailDataLandingPage = DB::table('site_infos')->where('id', $id)->first();
        return view('edit-data-landing-page', [
            'detailDataLandingPage' => $detailDataLandingPage,
        ]);
    }

    public function hapusDataLandingPage($id)
    {
       DB::table('site_infos')->where('id', $id)->delete();
       return redirect()->route('data-landing-page');
    }

    public function terapkanEditDataLandingPage(Request $request, $id)
    {
        DB::table('site_infos')->where('id', $id)->update([
            'flags' => $request->input('flags'),
            'konten' => $request->input('konten'),
            'updated_at' => Carbon::now('Asia/Bangkok')]);

            return redirect()->route('data-landing-page');
    }


}
