<?php

namespace App\Http\Controllers;

use App\Models\UserReview;
use App\Http\Requests\StoreUserReviewRequest;
use App\Http\Requests\UpdateUserReviewRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UserReviewController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataReviewPasar()
    {
        $daftarDataReviewPasar = DB::table('user_reviews')->get();
        $totalKios = DB::table('kiosks')->count();
        $totalKiosTersewa = DB::table('kiosks')->where('kiosk_status', 'tersewa')->count();
        $rataanSewa = DB::table('kiosks')->avg('kiosk_rent_price');
        $sewaTerbaru = DB::table('kiosks')->orderBy('created_at', 'desc')->first();
        return view('data-review-pasar', [
            'daftarDataReviewPasar' => $daftarDataReviewPasar,
            'totalKios' => $totalKios,
            'totalKiosTersewa' => $totalKiosTersewa,
            'rataanSewa' => $rataanSewa,
            'sewaTerbaru' => $sewaTerbaru,
        ]);
    }
    
    public function detailDataReviewPasar($id)
    {
        $detailDataReviewPasar = DB::table('user_reviews')->where('id', $id)->first();
        return view('detail-data-review-pasar', [
            'detailDataReviewPasar' => $detailDataReviewPasar,
        ]);
    }

    public function tambahDataReviewPasar()
    {
        return view('tambah-data-review-pasar', [
        ]);
    }

    public function terapkanTambahDataReviewPasar(Request $request)
    {
        $entry = array();
        $entry['reviewers_name'] = $request->input('reviewers_name');
        $entry['reviewers_phone_number'] = $request->input('reviewers_phone_number');
        $entry['reviewers_email'] = $request->input('reviewers_email');
        $entry['review_overall_score'] = $request->input('review_overall_score');
        $entry['review_comment'] = $request->input('review_comment');
        $entry['created_at'] = Carbon::now('Asia/Bangkok');
        $entry['updated_at'] = Carbon::now('Asia/Bangkok'); 
        DB::table('user_reviews')->insert($entry);

        return redirect()->route('data-review-pasar');
    }

    public function editDataReviewPasar($id)
    {
        $detailDataReviewPasar = DB::table('user_reviews')->where('id', $id)->first();
        return view('edit-data-review-pasar', [
            'detailDataReviewPasar' => $detailDataReviewPasar,
        ]);
    }

    public function hapusDataReviewPasar($id)
    {
       DB::table('user_reviews')->where('id', $id)->delete();
       return redirect()->route('data-review-pasar');
    }

    public function terapkanEditDataReviewPasar(Request $request, $id)
    {
        DB::table('user_reviews')->where('id', $id)->update([
            'reviewers_name' => $request->input('reviewers_name'),
            'reviewers_phone_number' => $request->input('reviewers_phone_number'),
            'reviewers_email' => $request->input('reviewers_email'),
            'review_overall_score' => $request->input('review_overall_score'),
            'review_comment' => $request->input('review_comment'),
            'updated_at' => Carbon::now('Asia/Bangkok')]);

            return redirect()->route('data-review-pasar');
    }
}
