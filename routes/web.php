<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\SiteInfoController::class, 'landingPage'])->name('landing-page');
Route::post('/terapkan-tambah-data-review-pasar-dari-landing-page', [App\Http\Controllers\SiteInfoController::class, 'tulisReviewDariLandingPage'])->name('terapkan-tambah-data-review-pasar-dari-landing-page');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');

// Route-route untuk olah data kios.
    // Detail Kios
    Route::get('/detail-kios/{id}', [App\Http\Controllers\HomeController::class, 'detailKios'])->name('detail-kios');
    // Tambah Kios
    Route::get('/tambah-kios', [App\Http\Controllers\HomeController::class, 'tambahKios'])->name('tambah-kios');
    Route::post('/terapkan-tambah-kios', [App\Http\Controllers\HomeController::class, 'terapkanTambahKios'])->name('terapkan-tambah-kios');
    // Edit Kios
    Route::get('/edit-kios/{id}', [App\Http\Controllers\HomeController::class, 'editKios'])->name('edit-kios');
    Route::put('/terapkan-edit-kios/{id}', [App\Http\Controllers\HomeController::class, 'terapkanEditKios'])->name('terapkan-edit-kios');
    //Hapus Kios
    Route::delete('/hapus-kios/{id}', [App\Http\Controllers\HomeController::class, 'hapusKios'])->name('hapus-kios');

// Route-route untuk olah data landing page
    // Daftar data landing page
    Route::get('/data-landing-page', [App\Http\Controllers\HomeController::class, 'dataLandingPage'])->name('data-landing-page');
    // Detail data landing page
    Route::get('/detail-data-landing-page/{id}', [App\Http\Controllers\HomeController::class, 'detailDataLandingPage'])->name('detail-data-landing-page');
    // Tambah data landing page
    Route::get('/tambah-data-landing-page', [App\Http\Controllers\HomeController::class, 'tambahDataLandingPage'])->name('tambah-data-landing-page');
    Route::post('/terapkan-tambah-data-landing-page', [App\Http\Controllers\HomeController::class, 'terapkanTambahDataLandingPage'])->name('terapkan-tambah-data-landing-page');
    // Edit data landing page
    Route::get('/edit-data-landing-page/{id}', [App\Http\Controllers\HomeController::class, 'editDataLandingPage'])->name('edit-data-landing-page');
    Route::put('/terapkan-edit-data-landing-page/{id}', [App\Http\Controllers\HomeController::class, 'terapkanEditDataLandingPage'])->name('terapkan-edit-data-landing-page');
    //Hapus data landing page
    Route::delete('/hapus-data-landing-page/{id}', [App\Http\Controllers\HomeController::class, 'hapusDataLandingPage'])->name('hapus-data-landing-page');

// Route-route untuk olah data review pasar
    // Daftar data review pasar
    Route::get('/data-review-pasar', [App\Http\Controllers\UserReviewController::class, 'dataReviewPasar'])->name('data-review-pasar');
    // Detail data landing page
    Route::get('/detail-data-review-pasar/{id}', [App\Http\Controllers\UserReviewController::class, 'detailDataReviewPasar'])->name('detail-data-review-pasar');
    // Tambah data review pasar
    Route::get('/tambah-data-review-pasar', [App\Http\Controllers\UserReviewController::class, 'tambahDataReviewPasar'])->name('tambah-data-review-pasar');
    Route::post('/terapkan-tambah-data-review-pasar', [App\Http\Controllers\UserReviewController::class, 'terapkanTambahDataReviewPasar'])->name('terapkan-tambah-data-review-pasar');
    // Edit data review pasar
    Route::get('/edit-data-review-pasar/{id}', [App\Http\Controllers\UserReviewController::class, 'editDataReviewPasar'])->name('edit-data-review-pasar');
    Route::put('/terapkan-edit-data-review-pasar/{id}', [App\Http\Controllers\UserReviewController::class, 'terapkanEditDataReviewPasar'])->name('terapkan-edit-data-review-pasar');
    //Hapus data review pasar
    Route::delete('/hapus-data-review-pasar/{id}', [App\Http\Controllers\UserReviewController::class, 'hapusDataReviewPasar'])->name('hapus-data-review-pasar');